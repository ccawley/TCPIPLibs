# Copyright 1995 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for socklib5

COMPONENT = socklib5

OBJS = accept5 bind connect copyerror getpeerna5 getsockna5 getsockopt \
       getstabsiz listen recv5 recvfrom5 recvmsg5 select send5 sendmsg5 \
       sendto setsockopt shutdown sockclose socket socketstat sockioctl \
       sockread sockreadv sockwrite sockwritev sendtosm sysctl oaccept \
       ogetpeerna ogetsockna orecvmsg osendmsg orecvfrom kvm_nlist sockver \
       _inet_err

include StdTools
include common${EXT}mk

# Dynamic dependencies:
